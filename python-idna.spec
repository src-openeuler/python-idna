%global _empty_manifest_terminate_build 0
Name:		python-idna
Version:	3.10
Release:	1
Summary:	Internationalized Domain Names in Applications (IDNA)
License:	BSD-3-Clause
URL:		https://github.com/kjd/idna
Source0:	https://pypi.io/packages/source/i/idna/idna-%{version}.tar.gz
BuildArch:	noarch

%description
A library to support the Internationalised Domain Names in
Applications (IDNA) protocol as specified in RFC 5891
http://tools.ietf.org/html/rfc5891. This version of the protocol
is often referred to as “IDNA2008” and can produce different
results from the earlier standard from 2003.

%package -n python3-idna
Summary:	Internationalized Domain Names in Applications (IDNA)
Provides:	python-idna = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-setuptools_scm
BuildRequires:	python3-pbr
BuildRequires:	python3-pip
BuildRequires:	python3-wheel
BuildRequires:	python3-flit-core pytest
%description -n python3-idna
A library to support the Internationalised Domain Names in
Applications (IDNA) protocol as specified in RFC 5891
http://tools.ietf.org/html/rfc5891. This version of the protocol
is often referred to as “IDNA2008” and can produce different
results from the earlier standard from 2003.

%package help
Summary:	Development documents and examples for idna
Provides:	python3-idna-doc
%description help
A library to support the Internationalised Domain Names in
Applications (IDNA) protocol as specified in RFC 5891
http://tools.ietf.org/html/rfc5891. This version of the protocol
is often referred to as “IDNA2008” and can produce different
results from the earlier standard from 2003.

%prep
%autosetup -n idna-%{version} -p1

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
%pytest

%files -n python3-idna -f filelist.lst
%dir %{python3_sitelib}/*
%{python3_sitelib}/idna/__pycache__/

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Mar 4 2025 zhangpan <zhangpan103@h-partners.com> - 3.10-1
- update to 3.10

* Fri Jun 14 2024 xinghe <xinghe2@h-partners.com> - 3.6-4
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix license

* Sat Apr 20 2024 zhangpan <zhangpan103@h-partners.com> - 3.6-3
- fix CVE-2024-3651

* Sat Apr 20 2024 zhangpan <zhangpan103@h-partners.com> - 3.6-2
- correct the tar package

* Mon Jan 15 2024 zhangpan <zhangpan103@h-partners.com> - 3.6-1
- update to 3.6

* Fri Jun 30 2023 li-miaomiao_zhr <mmlidc@isoftstone.com> - 3.4-1
- Update to 3.4

* Tue Nov 29 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 3.3-2
- revent to 3.3

* Wed Nov 23 2022 liqiuyu <liqiuyu@kylinos.cn> - 3.4-1
- Update package to version 3.4

* Tue May 24 2022 renliang <renliang@uniontech.com> - 3.3-1
- Upgrade package python3-idna to version 3.3

* Fri Dec 03 2021 wangkerong <wangkerong@huawei.com> - 3.2-1
- update to 3.2

* Thu Oct 29 2020 wangye <wangye70@huawei.com> - 2.10-2
- remove python2-idna subpackage

* Thu Jul 23 2020 zhouhaibo <zhouhaibo@huawei.com> - 2.10-1
- Package update

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.8-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:delete the python-idna

* Tue Jan 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.8-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:delete the python provides in python2

* Wed Sep 4 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.8-1
- Package init
